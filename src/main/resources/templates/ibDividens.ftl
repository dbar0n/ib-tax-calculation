<!DOCTYPE html>

<html lang="uk-ua">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

</head>


<div class="card card-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-4">
                <h4 class="card-title">
                    Виплачені дивіденди
                </h4>
            </div>

        </div>
    </div>
    <table class="table table-striped table-hover table-responsive">
        <thead>
        <tr>
            <th scope="col">Символ</th>
            <th scope="col">Дата виплати</th>
            <th scope="col">За акцию $</th>
            <th scope="col">Дохід $</th>
            <th scope="col">Курс гривні</th>
            <th scope="col">Дохід ₴</th>
            <th scope="col">ПДФО (9%)</th>
            <th scope="col">Військовий збір (1,5%)</th>
        </tr>
        </thead>
        <tbody>
        <#list dividends as dividend>
            <tr>
                <td>
                    <a href="https://wallmine.com/NASDAQ/${dividend.symbol}"
                       target="_blank">${dividend.symbol}</a>
                </td>
                <td>${dividend.date} </td>
                <td><span>${dividend.perShare}</span></td>
                <td><span>${dividend.amount}</span></td>
                <td>${dividend.rate}</td>
                <td><span>${dividend.amountUah}</span></td>
                <td><span>${dividend.pdfoUah}</span></td>
                <td><span>${dividend.militaryUah}</span></td>

            </tr>
        </#list>
        </tbody>
        <tfoot>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"> $ ${totalAmount}</th>
        <th scope="col"></th>
        <th scope="col">${totalUah} ₴</th>
        <th scope="col">${pdfoUah} ₴</th>
        <th scope="col">${militaryUah} ₴</th>
        </tfoot>
    </table>

</div>

