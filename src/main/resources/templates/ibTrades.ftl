<!DOCTYPE html>

<html lang="uk-ua">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

</head>

<div class="card card-body">
    <h4 class="card-title">Закриті ордери</h4>
    <table class="table table-striped table-hover table-responsive">
        <thead>
        <tr>
            <th scope="col">Символ</th>
            <th scope="col">Дохід $</th>
            <th scope="col">Дохід ₴</th>
            <th/>
            <th/>
            <th/>
            <th/>
            <th/>
            <th/>
            <th/>
            <th/>
        </tr>
        </thead>
        <tbody>
        <#list tradeSummaries as tradeSummary>
            <tr>
                <td>
                    <a href="https://wallmine.com/NASDAQ/${tradeSummary.symbol}"
                       target="_blank">${tradeSummary.symbol}</a>
                </td>


                <td><span>${tradeSummary.profitOrLost}</span></td>
                <td><span>${tradeSummary.profitOrLostInCurrency}</span></td>
                <td colspan="8">
                    <table>
                        <thead>
                        <tr>
                            <th scope="col">Кво</th>
                            <th scope="col">Дата Покупки</th>
                            <th scope="col">Цена Покупки</th>
                            <th scope="col">Курс Покупки</th>
                            <th scope="col">Сумма Покупки</th>
                            <th scope="col">Дата Продажи</th>
                            <th scope="col">Цена Продажи</th>
                            <th scope="col">Курс Продажи</th>
                            <th scope="col">Сумма Продажи</th>

                            <th scope="col">Коммисия</th>

                        </tr>
                        </thead>
                        <tbody>
                        <#list tradeSummary.tradeSummaryRows as row>
                            <tr>

                                <td><span>${row.quantity}</span></td>
                                <td><span>${row.buyDateTime!}</span></td>
                                <td><span>${row.buyTransactionPrice}</span></td>
                                <td><span>${row.buyRate}</span></td>
                                <td><span>${row.buySum}</span></td>
                                <td><span>${row.sellDateTime}</span></td>
                                <td><span>${row.sellTransactionPrice}</span></td>
                                <td><span>${row.sellRate}</span></td>
                                <td><span>${row.sellSum}</span></td>
                                <td><span>${row.commission}</span></td>

                            </tr>
                        </#list>

                        </tbody>
                    </table>
                </td>
            </tr>
          <#-- <tr colspan="6">

            </tr>-->
        </#list>

        </tbody>
    </table>

</div>

