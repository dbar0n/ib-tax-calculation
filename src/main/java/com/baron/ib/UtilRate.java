package com.baron.ib;

import com.baron.ib.model.Rate;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;

public class UtilRate {
    private static final String USER_AGENT = "Mozilla/5.0";

    private UtilRate() {
    }

    public static Rate getRate(LocalDate now, String currency) {
        String url = getRateUrl(now, currency);
        return query(url);

    }

    private static Rate query(String url) {
        try {
            URL obj = new URL(url);
            HttpURLConnection httpURLConnection = (HttpURLConnection) obj.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setRequestProperty("User-Agent", USER_AGENT);
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                // print result
                String x = response.toString();
                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                Object[] rates = objectMapper.readValue(x, Object[].class);
                LinkedHashMap<String, Object> rate = (LinkedHashMap<String, Object>) rates[0];
                String cc = rate.get("cc").toString();
                Double rateDouble = (Double) rate.get("rate");
                BigDecimal bigDecimal = BigDecimal.valueOf(rateDouble);
                return new Rate(bigDecimal, cc);

            } else {
                return null;
            }

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    private static String getRateUrl(LocalDate now, String currency) {
        String date = now.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        return String.format("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=%s&date=%s&json", currency, date);
    }
}
