package com.baron.ib.controller;

import com.baron.ib.service.DividendService;
import com.baron.ib.service.IbFilesService;
import freemarker.template.TemplateException;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.util.Map;

@Controller
public class FileController {


    private final IbFilesService ibFilesService;
    private final DividendService dividendService;

    public FileController(IbFilesService ibFilesService, DividendService dividendService) {
        this.ibFilesService = ibFilesService;
        this.dividendService = dividendService;
    }

    @RequestMapping("/")
    public String uploading(Model model) {
        model.addAttribute("files", ibFilesService.content());
        return "uploading";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String uploadingPost(@RequestParam("uploadingFile") MultipartFile uploadingFile,
                                @RequestParam(value = "year") Integer year, Model model) throws IOException, TemplateException {
        Map<String, Object> templateData = dividendService.process(uploadingFile, year);
        model.addAllAttributes(templateData);
        ibFilesService.uploadToIbBucket(uploadingFile,  templateData);
        return "ib";
    }

    @GetMapping(value = "/download/{key}", produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public String download(@PathVariable String key) {
        return new String(ibFilesService.downloadFromIbBucket(key));

    }

    @GetMapping(value = "/download/file/{key}", produces = MediaType.TEXT_HTML_VALUE)
    @ResponseBody
    public void downloadAsFile(@PathVariable String key, HttpServletResponse response) throws IOException {
        byte[] download = ibFilesService.downloadFromIbBucket(key);
        String mimeType = URLConnection.guessContentTypeFromName(key);
        if (mimeType == null) {
            mimeType = "application/octet-stream";
        }
        response.setContentType(mimeType);
        response.setHeader("Content-Disposition", "inline; filename=\"" + key + "\"");
        response.setContentLength(download.length);
        InputStream inputStream = new ByteArrayInputStream(download);
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }

}

