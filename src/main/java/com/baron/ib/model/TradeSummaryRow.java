package com.baron.ib.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class TradeSummaryRow {
    private final String  symbol;
    private final BigDecimal buySum;
    private final BigDecimal sellSum;
    private final BigDecimal profit;
    BigDecimal quantity;
    BigDecimal commission;
    BigDecimal buyTransactionPrice;
    BigDecimal buyRate;
    BigDecimal sellRate;
    BigDecimal sellTransactionPrice;
    LocalDate sellDateTime;
    LocalDate buyDateTime;

    public TradeSummaryRow(String symbol,BigDecimal quantity, BigDecimal commission,
                           BigDecimal buyTransactionPrice, BigDecimal buyRate,
                           BigDecimal sellRate, BigDecimal sellTransactionPrice,
                           LocalDateTime sellDateTime, LocalDateTime buyDateTime) {
        this.symbol = symbol;
        this.quantity = quantity;
        this.commission = commission;
        this.buyTransactionPrice = buyTransactionPrice;
        this.buyRate = buyRate;
        this.buySum = buyTransactionPrice.multiply(buyRate).multiply(quantity).setScale(2, RoundingMode.HALF_UP);
        this.sellRate = sellRate;
        this.sellTransactionPrice = sellTransactionPrice;
        this.sellSum = sellTransactionPrice.multiply(sellRate).multiply(quantity).setScale(2,RoundingMode.HALF_UP);
        this.sellDateTime = sellDateTime.toLocalDate();
        this.buyDateTime = buyDateTime.toLocalDate();
        this.profit = sellSum.subtract(buySum);
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public BigDecimal getBuyTransactionPrice() {
        return buyTransactionPrice;
    }

    public void setBuyTransactionPrice(BigDecimal buyTransactionPrice) {
        this.buyTransactionPrice = buyTransactionPrice;
    }

    public BigDecimal getBuyRate() {
        return buyRate;
    }

    public void setBuyRate(BigDecimal buyRate) {
        this.buyRate = buyRate;
    }

    public BigDecimal getSellRate() {
        return sellRate;
    }

    public void setSellRate(BigDecimal sellRate) {
        this.sellRate = sellRate;
    }

    public BigDecimal getSellTransactionPrice() {
        return sellTransactionPrice;
    }

    public void setSellTransactionPrice(BigDecimal sellTransactionPrice) {
        this.sellTransactionPrice = sellTransactionPrice;
    }

    public LocalDate getSellDateTime() {
        return sellDateTime;
    }

    public void setSellDateTime(LocalDate sellDateTime) {
        this.sellDateTime = sellDateTime;
    }

    public LocalDate getBuyDateTime() {
        return buyDateTime;
    }

    public void setBuyDateTime(LocalDate buyDateTime) {
        this.buyDateTime = buyDateTime;
    }

    public BigDecimal getBuySum() {
        return buySum;
    }

    public BigDecimal getSellSum() {
        return sellSum;
    }

    public BigDecimal getProfit() {
        return profit;
    }

    public String getSymbol() {
        return symbol;
    }
}
