package com.baron.ib.model;

public class TradeRaw {
    String trades; //Trades,
    String header; //Header,
    String dataDiscriminator; //DataDiscriminator,
    String assetCategory; //Asset Category,
    String currency; //Currency,
    String symbol; //Symbol,
    String dateTime; //Date/Time,
    String quantity; //Quantity,
    String transactionPrice; //T.Price,
    String closePrice; //C.Price,
    String proceeds; //Proceeds,
    String commissionFee; //Comm/Fee,
    String basis; //Basis,
    String realizedPL; //Realized P/L,
    String marketPL; //MTM P/L,
    String code;//Code

    public TradeRaw(String trades, String header, String dataDiscriminator, String assetCategory, String currency, String symbol, String dateTime, String quantity, String transactionPrice, String closePrice, String proceeds, String commissionFee, String basis, String realizedPL, String marketPL, String code) {
        this.trades = trades;
        this.header = header;
        this.dataDiscriminator = dataDiscriminator;
        this.assetCategory = assetCategory;
        this.currency = currency;
        this.symbol = symbol;
        this.dateTime = dateTime;
        this.quantity = quantity;
        this.transactionPrice = transactionPrice;
        this.closePrice = closePrice;
        this.proceeds = proceeds;
        this.commissionFee = commissionFee;
        this.basis = basis;
        this.realizedPL = realizedPL;
        this.marketPL = marketPL;
        this.code = code;
    }

    public TradeRaw(String[] s) {
        this(s[0],
            s[1],
            s[2],
            s[3],
            s[4],
            s[5],
            s[6].replace("\"", ""),
            s[7],
            s[8],
            s[9],
            s[10],
            s[11],
            s[12],
            s[13],
            s[14],
            s[15]);
    }

    public String trades() {
        return trades;
    }

    public String header() {
        return header;
    }

    public String dataDiscriminator() {
        return dataDiscriminator;
    }

    public String assetCategory() {
        return assetCategory;
    }

    public String currency() {
        return currency;
    }

    public String symbol() {
        return symbol;
    }

    public String dateTime() {
        return dateTime;
    }

    public String quantity() {
        return quantity;
    }

    public String transactionPrice() {
        return transactionPrice;
    }

    public String closePrice() {
        return closePrice;
    }

    public String proceeds() {
        return proceeds;
    }

    public String commissionFee() {
        return commissionFee;
    }

    public String basis() {
        return basis;
    }

    public String realizedPL() {
        return realizedPL;
    }

    public String marketPL() {
        return marketPL;
    }

    public String code() {
        return code;
    }
}
