package com.baron.ib.model;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class TradeSummary {
    protected static final Logger logger = LoggerFactory.getLogger(TradeSummary.class);
    String symbol;
    BigDecimal profitOrLost;
    BigDecimal profitOrLostInCurrency;
    List<TradeSummaryRow> tradeSummaryRows = new ArrayList<>();


    public TradeSummary(String symbol, List<Trade> trades, int year) {
        //System.out.println(symbol);
        // trades.forEach(x -> System.out.println(x.toString()));
        //  System.out.println("-----------------------------------");
        TreeSet<Trade> buys = trades.stream().filter(x -> x.quantity().compareTo(BigDecimal.ZERO) > 0)
            .collect(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(Trade::dateTime))));
        List<Trade> sells = trades.stream()
            .filter(x -> x.quantity().compareTo(BigDecimal.ZERO) < 0)
            .filter(x -> x.dateTime().getYear() == year)
            .collect(Collectors.toList());
        BigDecimal total = BigDecimal.ZERO;
        BigDecimal totalRate = BigDecimal.ZERO;
        for (Trade sell : sells) {
            BigDecimal quantity = sell.quantity().abs();
            BigDecimal baseBuy = BigDecimal.ZERO;
            BigDecimal baseBuyRate = BigDecimal.ZERO;
            BigDecimal sellCommissionUah = sell.commissionFee().abs().multiply(sell.rate().rate());
            for (Trade buy : buys) {
                BigDecimal buyQnt = buy.quantity();
                if (buyQnt.compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }
                BigDecimal min = min(quantity, buyQnt);
                BigDecimal parSum = min.multiply(buy.transactionPrice());
                BigDecimal buyCommissionUsd = buy.commissionFee().multiply(quantity).divide(buy.quantity(), 6, RoundingMode.HALF_UP);
                baseBuy = baseBuy.add(parSum).subtract(buyCommissionUsd);
                BigDecimal buyCommissionUah = buyCommissionUsd.multiply(buy.rate().rate());
                baseBuyRate = baseBuyRate.add(parSum.multiply(buy.rate().rate())).subtract(buyCommissionUah);
                buy.subtractQnt(min);
                tradeSummaryRows.add(new TradeSummaryRow(symbol, min,
                    sellCommissionUah.subtract(buyCommissionUah).multiply(BigDecimal.valueOf(-1)),
                    buy.transactionPrice(),
                    buy.rate().rate(),
                    sell.rate.rate(), sell.transactionPrice(), sell.dateTime, buy.dateTime));
                quantity = quantity.subtract(min);
                if (quantity.compareTo(BigDecimal.ZERO) == 0) {
                    break;
                }
            }
            BigDecimal sumOfSale = sell.quantity().abs().multiply(sell.transactionPrice());
            total = total.add(sumOfSale);
            total = total.subtract(sell.commissionFee().abs());
            total = total.subtract(baseBuy);
            totalRate = totalRate.add(sumOfSale.multiply(sell.rate().rate()));
            totalRate = totalRate.subtract(sellCommissionUah);
            totalRate = totalRate.subtract(baseBuyRate);
            this.symbol = symbol;
            this.profitOrLost = total.setScale(2, RoundingMode.HALF_UP);
            this.profitOrLostInCurrency = totalRate.setScale(2, RoundingMode.HALF_UP);
        }

    }

    BigDecimal min(BigDecimal a, BigDecimal b) {
        return a.compareTo(b) < 0 ? a : b;
    }

    public String getSymbol() {
        return symbol;
    }

    public BigDecimal getProfitOrLost() {
        return profitOrLost;
    }

    public BigDecimal getProfitOrLostInCurrency() {
        return profitOrLostInCurrency;
    }

    @Override
    public String toString() {
        return String.format("TradeSummary(%-4s, %8.2f$,  %8.2f₴)", symbol, profitOrLost, profitOrLostInCurrency);
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public void setProfitOrLost(BigDecimal profitOrLost) {
        this.profitOrLost = profitOrLost;
    }

    public void setProfitOrLostInCurrency(BigDecimal profitOrLostInCurrency) {
        this.profitOrLostInCurrency = profitOrLostInCurrency;
    }

    public List<TradeSummaryRow> getTradeSummaryRows() {
        return tradeSummaryRows;
    }

    public void setTradeSummaryRows(List<TradeSummaryRow> tradeSummaryRows) {
        this.tradeSummaryRows = tradeSummaryRows;
    }


    public int getTradeSummaryRowsSize() {
        return tradeSummaryRows.size();
    }


}
