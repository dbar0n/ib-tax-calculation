package com.baron.ib.model;

import com.baron.ib.service.UtilRate;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Trade {
    String currency;
    String symbol;
    LocalDateTime dateTime;
    BigDecimal quantity;
    BigDecimal transactionPrice;
    BigDecimal closePrice;
    BigDecimal proceeds;
    BigDecimal commissionFee;
    BigDecimal basis;
    BigDecimal realizedPL;
    BigDecimal marketPL;
    String code;
    Rate rate;


    public Trade(String currency, String symbol, LocalDateTime dateTime,
                 BigDecimal quantity, BigDecimal transactionPrice,
                 BigDecimal closePrice, BigDecimal proceeds, BigDecimal commissionFee,
                 BigDecimal basis, BigDecimal realizedPL, BigDecimal marketPL, String code, Rate rate) {
        this.currency = currency;
        this.symbol = symbol;
        this.dateTime = dateTime;
        this.quantity = quantity;
        this.transactionPrice = transactionPrice;
        this.closePrice = closePrice;
        this.proceeds = proceeds;
        this.commissionFee = commissionFee;
        this.basis = basis;
        this.realizedPL = realizedPL;
        this.marketPL = marketPL;
        this.code = code;
        this.rate = rate;
    }

    public Trade(String currency, String symbol, String dateTime, double quantity, double transactionPrice, double closePrice, double proceeds, double commissionFee, double basis, double realizedPL, double marketPL, String code, Rate rate) {
        this.currency = currency;
        this.symbol = symbol;
        this.dateTime = LocalDateTime.parse(dateTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
        this.quantity = BigDecimal.valueOf(quantity);
        this.transactionPrice = BigDecimal.valueOf(transactionPrice);
        this.closePrice = BigDecimal.valueOf(closePrice);
        this.proceeds = BigDecimal.valueOf(proceeds);
        this.commissionFee = BigDecimal.valueOf(commissionFee);
        this.basis = BigDecimal.valueOf(basis);
        this.realizedPL = BigDecimal.valueOf(realizedPL);
        this.marketPL = BigDecimal.valueOf(marketPL);
        this.code = code;
        this.rate = rate;
    }

    public void subtractQnt(BigDecimal subtraction) {
        commissionFee = quantity.subtract(subtraction).multiply(commissionFee).divide(quantity);
        quantity = quantity.subtract(subtraction);
    }

    public String currency() {
        return this.currency;
    }

    public String symbol() {
        return this.symbol;
    }

    public LocalDateTime dateTime() {
        return this.dateTime;
    }

    public BigDecimal quantity() {
        return this.quantity;
    }

    public BigDecimal transactionPrice() {
        return this.transactionPrice;
    }

    public BigDecimal closePrice() {
        return this.closePrice;
    }

    public BigDecimal proceeds() {
        return this.proceeds;
    }

    public BigDecimal commissionFee() {
        return this.commissionFee;
    }

    public BigDecimal basis() {
        return this.basis;
    }

    public BigDecimal realizedPL() {
        return this.realizedPL;
    }

    public BigDecimal marketPL() {
        return this.marketPL;
    }

    public String code() {
        return this.code;
    }

    public Rate rate() {
        return this.rate;
    }

    @Override
    public String toString() {
        return String.format(" Trade(%-4s, %-3s, %tF, qnt=%4.0f, price=%8.2f, proceeds=%8.2f, fee=%8.6f rate=%s)",
            symbol, currency, dateTime, quantity, transactionPrice, proceeds, commissionFee, rate.toString());

    }
}
