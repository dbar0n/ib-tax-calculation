package com.baron.ib.model;

import com.baron.ib.service.UtilRate;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Dividend {
    String currency;
    LocalDate date;
    BigDecimal amount;
    BigDecimal perShare;
    String symbol;
    String description;
    Rate rate;

    public Dividend(DividendRaw r,Rate rate) {
        this.currency = r.currency();
        this.date = LocalDate.parse(r.date(), DateTimeFormatter.ISO_DATE); // 2020-09-24
        this.amount = new BigDecimal(r.amount());
        this.perShare = getPerShare(r.description());
        this.symbol = getTicker(r.description());
        this.description = r.description();
        this.rate = rate;
    }


    private static BigDecimal getPerShare(String dsc) {
        String[] split = dsc.split("USD");
        String[] split1 = StringUtils.trim(split[split.length - 1]).split("\\s");
        String val = split1[0];
        return new BigDecimal(val);
    }

    private static String getTicker(String dsc) {
        return dsc.split("\\(")[0];
    }

    public String currency() {
        return currency;
    }

    public LocalDate date() {
        return date;
    }

    public BigDecimal amount() {
        return amount;
    }

    public BigDecimal perShare() {
        return perShare;
    }

    public String symbol() {
        return symbol;
    }

    public String description() {
        return description;
    }

    public Rate rate() {
        return rate;
    }


}
