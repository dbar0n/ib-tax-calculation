package com.baron.ib.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DividendDto {
    Dividend dividend;
    String currency;
    String date;
    String amount;
    String perShare;
    String symbol;
    String description;
    String rate;
    String amountUah;
    String pdfoUah;
    String militaryUah;


    public DividendDto(Dividend dividend) {
        this.currency = dividend.currency();
        this.date = dividend.date().toString();
        this.amount = dividend.amount().toString();
        this.perShare = dividend.perShare().toString();
        this.symbol = dividend.symbol();
        this.description = dividend.description();
        this.rate = dividend.rate().rate().toString();
        BigDecimal multiply = dividend.rate().rate().multiply(dividend.amount());
        this.amountUah = multiply.setScale(2, RoundingMode.HALF_UP).toString();
        this.pdfoUah = multiply.multiply(BigDecimal.valueOf(0.09d)).setScale(2, RoundingMode.HALF_UP).toString();
        this.militaryUah = multiply.multiply(BigDecimal.valueOf(0.015d)).setScale(2, RoundingMode.HALF_UP).toString();
    }

    public Dividend getDividend() {
        return dividend;
    }

    public void setDividend(Dividend dividend) {
        this.dividend = dividend;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPerShare() {
        return perShare;
    }

    public void setPerShare(String perShare) {
        this.perShare = perShare;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getAmountUah() {
        return amountUah;
    }

    public void setAmountUah(String amountUah) {
        this.amountUah = amountUah;
    }

    public String getPdfoUah() {
        return pdfoUah;
    }

    public void setPdfoUah(String pdfoUah) {
        this.pdfoUah = pdfoUah;
    }

    public String getMilitaryUah() {
        return militaryUah;
    }

    public void setMilitaryUah(String militaryUah) {
        this.militaryUah = militaryUah;
    }
}
