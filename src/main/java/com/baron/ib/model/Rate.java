package com.baron.ib.model;

import java.math.BigDecimal;

public class Rate {
    private BigDecimal rate;
    private String cc;


    public Rate(BigDecimal rate, String cc) {
        this.rate = rate;
        this.cc = cc;
    }


    public String toString() {
        return String.format("Rate(%-4s,%8.6f)", cc, rate);

    }

    public BigDecimal rate() {
        return rate;
    }

    public String cc() {
        return cc;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }
}





