package com.baron.ib.model;

import software.amazon.awssdk.services.s3.model.S3Object;

public class ReportFileResult {
    private final S3Object csvFile;
    private final S3Object resultFileHtml;
    private final S3Object resultFileXml;


    public ReportFileResult(S3Object csvFile, S3Object resultFileHtml, S3Object resultFileXml) {
        this.csvFile = csvFile;
        this.resultFileHtml = resultFileHtml;
        this.resultFileXml = resultFileXml;
    }

    public S3Object getCsvFile() {
        return csvFile;
    }

    public S3Object getResultFileHtml() {
        return resultFileHtml;
    }

    public S3Object getResultFileXml() {
        return resultFileXml;
    }
}
