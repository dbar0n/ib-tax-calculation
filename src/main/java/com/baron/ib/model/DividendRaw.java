package com.baron.ib.model;

public class DividendRaw {
    String dividends;
    String header;
    String currency;
    String date;
    String description;
    String amount;

    public DividendRaw() {
    }



    public DividendRaw(String[] s) {
        this(s[0], s[1], s[2], s[3], s[4], s[5]);
    }

    public DividendRaw(String dividends,
                       String header, String currency, String date, String description, String amount) {
        this.dividends = dividends;
        this.header = header;
        this.currency = currency;
        this.date = date;
        this.description = description;
        this.amount = amount;
    }

    public String dividends() {
        return dividends;
    }

    public String header() {
        return header;
    }

    public String currency() {
        return currency;
    }

    public String date() {
        return date;
    }

    public String description() {
        return description;
    }

    public String amount() {
        return amount;
    }


}
