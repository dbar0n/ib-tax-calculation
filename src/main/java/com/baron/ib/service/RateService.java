package com.baron.ib.service;

import com.baron.ib.model.Rate;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Set;

@Service
public class RateService {
    private static LinkedHashMap<LocalDate, Rate> rates = new LinkedHashMap<>();
    LocalDate startDay = LocalDate.of(2020, 1, 1);
    private final CollectionService collectionService;

    @Autowired
    ObjectMapper objectMapper;

    public RateService(CollectionService collectionService) {
        this.collectionService = collectionService;
    }


    @PostConstruct
    public void init() {
        loadFromCollection();
    }

    private void loadFromCollection() {
        LinkedHashMap<LocalDate, Rate> localDateRateLinkedHashMap =
            collectionService.getCollection("rates", new TypeReference<>() {
            });
        localDateRateLinkedHashMap = localDateRateLinkedHashMap == null ? rates : localDateRateLinkedHashMap;
        fillMapToDate(localDateRateLinkedHashMap, LocalDate.now());

    }

    private void fillMapToDate(LinkedHashMap<LocalDate, Rate> localDateRateLinkedHashMap, LocalDate toDate) {
        LocalDate iter = getLatest(localDateRateLinkedHashMap);
        boolean needToSave = false;
        while (iter.isBefore(toDate)) {
            iter = iter.plusDays(1);
            Rate rate = localDateRateLinkedHashMap.get(iter);
            if (rate == null) {
                rate = UtilRate.getRate(iter, "USD");
                localDateRateLinkedHashMap.put(iter, rate);
                needToSave = true;
            }
        }
        rates.putAll(localDateRateLinkedHashMap);
        if (needToSave) {
            collectionService.saveCollection("rates", rates);
        }
    }

    private LocalDate getLatest(LinkedHashMap<LocalDate, Rate> map) {
        Set<LocalDate> keys = map.keySet();
        LocalDate[] arr = keys.toArray(new LocalDate[0]);
        return arr.length == 0 ? startDay : arr[arr.length - 1];
    }

    public Rate getRate(LocalDate localDate) {
        Rate rate = rates.get(localDate);
        if (rate == null) {
            fillMapToDate(rates, localDate);

        }
        return rates.get(localDate);

    }
}
