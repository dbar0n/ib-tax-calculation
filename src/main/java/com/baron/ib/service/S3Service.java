package com.baron.ib.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.core.sync.ResponseTransformer;
import software.amazon.awssdk.core.waiters.WaiterResponse;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.services.s3.paginators.ListObjectsV2Iterable;
import software.amazon.awssdk.services.s3.waiters.S3Waiter;

import javax.annotation.PostConstruct;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class S3Service {
    private final Region region;
    private S3Client s3Client;
    @Value("${access.key.id}")
    String accessKeyId;
    @Value("${secret.access.key}")
    String secretAccessKey;

    public S3Service() {
        region = Region.EU_NORTH_1;

    }

    @PostConstruct
    public void init() {
        s3Client = S3Client.builder()
            .region(region)
            .credentialsProvider(() -> AwsBasicCredentials.create(accessKeyId, secretAccessKey))
            .build();
    }

    public void createBucket(String bucketName) {
        try {
            S3Waiter s3Waiter = s3Client.waiter();
            CreateBucketRequest bucketRequest = CreateBucketRequest.builder()
                .bucket(bucketName)
                .createBucketConfiguration(
                    CreateBucketConfiguration.builder()
                        .locationConstraint(region.id())
                        .build())
                .build();
            s3Client.createBucket(bucketRequest);
            HeadBucketRequest bucketRequestWait = HeadBucketRequest.builder()
                .bucket(bucketName)
                .build();
            // Wait until the bucket is created and print out the response
            WaiterResponse<HeadBucketResponse> waiterResponse = s3Waiter.waitUntilBucketExists(bucketRequestWait);
            waiterResponse.matched().response().ifPresent(System.out::println);
            System.out.println(bucketName + " is ready");

        } catch (S3Exception e) {
            System.err.println(e.awsErrorDetails().errorMessage());

        }
    }


    public void upload(byte[] bytes, String fileName, String bucketName) {
        upload(bytes, fileName, bucketName, false);
    }

    public void upload(byte[] bytes, String fileName, String bucketName, boolean encode) {
        PutObjectRequest objectRequest = PutObjectRequest.builder()
            .bucket(bucketName)
            .key(fileName)
            .build();
        byte[] encode1 = encode ? Base64.getEncoder().encode(bytes) : bytes;
        RequestBody requestBody = RequestBody.fromBytes(encode1);
        s3Client.putObject(objectRequest, requestBody);
        s3Client.putObject(objectRequest, requestBody);
    }


    public byte[] download(String key, String bucketName) {
        return download(key, bucketName, false);
    }

    public byte[] download(String key, String bucketName, boolean decode) {
        try {
            GetObjectRequest build = GetObjectRequest.builder().bucket(bucketName).key(key).build();
            ResponseBytes<GetObjectResponse> s3Object = s3Client.getObject(
                build,
                ResponseTransformer.toBytes());
            return decode ? Base64.getDecoder().decode(s3Object.asByteArray()) : s3Object.asByteArray();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new byte[0];
    }


    public List<ListObjectsV2Response> content(String bucketName) {
        ListObjectsV2Request listReq = ListObjectsV2Request.builder()
            .bucket(bucketName)
            .build();
        ListObjectsV2Iterable listRes = s3Client.listObjectsV2Paginator(listReq);
        return listRes.stream().collect(Collectors.toList());
    }


}
