package com.baron.ib.service;

import com.baron.ib.model.DividendRaw;
import com.baron.ib.model.TradeRaw;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Parser {
    private static final String COMMA_DELIMITER = ",(?=(?:(?:[^\"]*\"){2})*[^\"]*$)";
    private static final Logger logger = LoggerFactory.getLogger(Parser.class);

    private Parser() {
    }

    public static ParseResult getParserResult(String file) {
        List<String> lines = getStringStream(file);
        return getParserResult(lines);
    }

    public static ParseResult getParserResult(byte[] bytes) {
        String text = new String(bytes);
        String[] line = text.split("\\r?\\n");
        List<String> lines = Arrays.stream(line).collect(Collectors.toList());
        return getParserResult(lines);
    }

    public static ParseResult getParserResult(List<String> lines) {
        Stream<String> dividendsStream = lines.stream().filter(line -> line.startsWith("Dividends"));
        Stream<String> tradeRawStream = lines.stream().filter(line -> line.startsWith("Trades"));
        List<DividendRaw> dividendRaws = dividendsStream
            .map(line -> new DividendRaw(line.split(COMMA_DELIMITER)))
            .filter(d -> !Objects.equals(d.date(), "Date") && !Objects.equals(d.currency(), "Total"))
            .collect(Collectors.toList());
        //
        List<TradeRaw> tradeRaws = tradeRawStream
            .map(line -> new TradeRaw(Arrays.copyOf(line.split(COMMA_DELIMITER), 16)))
            .filter(t -> Objects.equals(t.header(), "Data") &&
                Objects.equals(t.dataDiscriminator(), "Order") &&
                Objects.equals(t.assetCategory(), "Stocks"))
            .collect(Collectors.toList());
        ParseResult parseResult = new ParseResult(dividendRaws, tradeRaws);
        return parseResult;
    }


    private static List<String> getStringStream(String file) {
        Stream<String> lines = null;
        try {
            Path path = Paths.get(file);
            lines = Files.lines(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert lines != null;
        List<String> collect = lines.collect(Collectors.toList());
        lines.close();
        return collect;
    }
}
