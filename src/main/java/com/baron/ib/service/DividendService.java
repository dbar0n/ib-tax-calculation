package com.baron.ib.service;

import com.baron.ib.model.*;
import freemarker.template.TemplateException;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DividendService {

    private final RateService rateService;

    public DividendService(RateService rateService) {
        this.rateService = rateService;
    }


    public Map<String, Object> process(MultipartFile file, int year) throws IOException {
        byte[] bytes = file.getBytes();
        ParseResult parserResult = Parser.getParserResult(bytes);
        List<Dividend> dividendList = parserResult.dividendRaws().stream().map(r -> {
            LocalDate date = LocalDate.parse(r.date(), DateTimeFormatter.ISO_DATE);
            Rate rate = rateService.getRate(date);
            return new Dividend(r, rate);
        }).collect(Collectors.toList());
        BigDecimal totalAmount = dividendList.stream().map(Dividend::amount).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal uah = dividendList.stream().map(dividend -> dividend.amount().multiply(dividend.rate().rate())).reduce(BigDecimal.ZERO, BigDecimal::add);
        List<Trade> trades = parserResult.tradeRaws().stream().map(r -> {
            LocalDateTime t = LocalDateTime.parse(r.dateTime().replace(", ", "T"),
                DateTimeFormatter.ISO_LOCAL_DATE_TIME);
            LocalDate now = t.toLocalDate();
            Rate rate = rateService.getRate(now);
            return new Trade(r.currency(),
                r.symbol(),
                t,
                new BigDecimal(r.quantity()),
                new BigDecimal(r.transactionPrice()),
                new BigDecimal(r.closePrice()),
                new BigDecimal(r.proceeds()),
                new BigDecimal(r.commissionFee()),
                new BigDecimal(r.basis()),
                new BigDecimal(r.realizedPL()),
                new BigDecimal(r.marketPL()),
                r.code(),
                rate);

        }).collect(Collectors.toList());
        Map<String, List<Trade>> tradesBySymbol = trades.stream()
            .collect(Collectors.groupingBy(Trade::symbol));
        Map<String, List<Trade>> tradeWhereWasSell = tradesBySymbol
            .entrySet().stream()
            .filter(e -> e.getValue().stream().anyMatch(t -> BigDecimal.ZERO.compareTo(t.quantity()) > 0))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        List<TradeSummary> tradeSummaries = tradeWhereWasSell.entrySet().stream()
            .map(e -> new TradeSummary(e.getKey(), e.getValue(), year))
            .sorted(Comparator.comparing(TradeSummary::getSymbol))
            .collect(Collectors.toList());
        BigDecimal profitOrLost = tradeSummaries.stream()
            .map(TradeSummary::getProfitOrLost)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal profitOrLostInCurrency = tradeSummaries.stream()
            .map(TradeSummary::getProfitOrLostInCurrency)
            .reduce(BigDecimal.ZERO, BigDecimal::add);
        Map<String, Object> templateData = new HashMap<>();
        templateData.put("dividends", dividendList.stream().map(DividendDto::new).collect(Collectors.toList()));
        templateData.put("totalAmount", totalAmount);
        templateData.put("totalUah", uah);
        templateData.put("pdfoUah", uah.multiply(BigDecimal.valueOf(0.09d)).setScale(2, RoundingMode.HALF_UP));
        templateData.put("militaryUah", uah.multiply(BigDecimal.valueOf(0.015d)).setScale(2, RoundingMode.HALF_UP));
        templateData.put("tradeSummaries", tradeSummaries);
        templateData.put("tradeSummaryRows", tradeSummaries.stream().flatMap(x -> x.getTradeSummaryRows().stream()).collect(Collectors.toList()));
        BigDecimal sellTotalWithCommission = tradeSummaries.stream().flatMap(x -> x.getTradeSummaryRows().stream())
            .map(x -> x.getSellSum().add(x.getCommission()))
            .map(x -> x.setScale(2, RoundingMode.HALF_UP))
            .reduce(BigDecimal.ZERO, BigDecimal::add);
        templateData.put("sellTotalWithCommission", sellTotalWithCommission);
        BigDecimal buyTotal = tradeSummaries.stream().flatMap(x -> x.getTradeSummaryRows().stream())
            .map(TradeSummaryRow::getBuySum)
            .map(x -> x.setScale(2, RoundingMode.HALF_UP))
            .reduce(BigDecimal.ZERO, BigDecimal::add);
        templateData.put("buyTotal", buyTotal);
        templateData.put("result", buyTotal.subtract(buyTotal));
        templateData.put("profitOrLost", profitOrLost);
        templateData.put("profitOrLostInCurrency", profitOrLostInCurrency);
        templateData.put("year", year);
        return templateData;


    }
}
