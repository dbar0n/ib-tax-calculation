package com.baron.ib.service;

import com.baron.ib.model.ReportFileResult;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;
import software.amazon.awssdk.services.s3.model.S3Object;

import java.io.IOException;
import java.io.StringWriter;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class IbFilesService {

    public static final String BUCKET_IB_FILES = "ibfile";
    private final S3Service s3service;
    @Autowired
    Configuration cfg;

    public IbFilesService(S3Service s3service) {
        this.s3service = s3service;
    }

    public void uploadToIbBucket(MultipartFile file, Map<String, Object> templateData) throws IOException, TemplateException {
        byte[] bytes = file.getBytes();
        String originalFilename = Optional.ofNullable(file.getOriginalFilename()).orElse("");
        long uploadId = Optional.<Long>empty().orElse(Instant.now().toEpochMilli());
        s3service.upload(bytes, uploadId + "_ib_" + originalFilename, BUCKET_IB_FILES);
        String inn = "2882122279";
        String docNumber = "F0100211";
        String nalogovaya = "1512";
        String year = templateData.get("year").toString();
        String xmlFileName = docNumber + ".xml";
        StringWriter xml = getRenderFreeMarker(templateData, "xml.ftlh");
        System.err.println(xml);
        uploadResultToIbBucket(xml.toString().getBytes(), xmlFileName, Optional.of(uploadId));
        StringWriter out = getRenderFreeMarker(templateData, "ib.ftlh");
        String fileName = originalFilename.replace(fileName(originalFilename), "html");
        uploadResultToIbBucket(out.toString().getBytes(), fileName, Optional.of(uploadId));

    }

    private StringWriter getRenderFreeMarker(Map<String, Object> templateData, String templateFileName) throws IOException, TemplateException {
        Template template = cfg.getTemplate(templateFileName);
        StringWriter out = new StringWriter();
        template.process(templateData, out);
        return out;
    }

    public long uploadResultToIbBucket(byte[] bytes, String fileName, Optional<Long> instant) {
        long l = instant.orElse(Instant.now().toEpochMilli());
        String ibResult = "_ib_result_";
        String newFileName = l + ibResult + fileName;
        if (newFileName.contains(ibResult)) {
            s3service.upload(bytes, newFileName, BUCKET_IB_FILES, true);
        } else {
            s3service.upload(bytes, newFileName, BUCKET_IB_FILES);
        }
        return l;
    }


    public byte[] downloadFromIbBucket(String key) {
        if (key.contains("_ib_result_")) {
            return s3service.download(key, BUCKET_IB_FILES, true);
        } else {
            return s3service.download(key, BUCKET_IB_FILES);
        }
    }

    static final class Result {
        private final String uploadId;
        private final S3Object s3Object;

        Result(String uploadId, S3Object s3Object) {
            this.uploadId = uploadId;
            this.s3Object = s3Object;
        }

        public String uploadId() {
            return uploadId;
        }

        public S3Object s3Object() {
            return s3Object;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) return true;
            if (obj == null || obj.getClass() != this.getClass()) return false;
            var that = (Result) obj;
            return Objects.equals(this.uploadId, that.uploadId) &&
                Objects.equals(this.s3Object, that.s3Object);
        }

        @Override
        public int hashCode() {
            return Objects.hash(uploadId, s3Object);
        }

        @Override
        public String toString() {
            return "Result[" +
                "uploadId=" + uploadId + ", " +
                "s3Object=" + s3Object + ']';
        }

    }

    public List<ReportFileResult> content() {
        List<ListObjectsV2Response> co = s3service.content(BUCKET_IB_FILES);
        Map<String, List<Result>> collect =
            co.stream()
                .flatMap(r -> r.contents().stream())
                .filter(c -> c.key().contains("_ib_"))
                .map(c -> new Result(getFirstPrefix(c.key()), c))
                .collect(Collectors.groupingBy(x -> x.uploadId));
        return collect.entrySet().stream()
            .map(x -> getReportFileResult(x.getKey(), x.getValue()))
            .collect(Collectors.toList());
    }

    private ReportFileResult getReportFileResult(String key, List<Result> value) {
        S3Object csvFile = value.stream()
            .map(x -> x.s3Object)
            .filter(x -> x.key().contains(".csv"))
            .findFirst().orElse(S3Object.builder().build());
        S3Object resultFileHtml = value.stream()
            .map(x -> x.s3Object)
            .filter(x -> x.key().contains(".html"))
            .findFirst().orElse(S3Object.builder().build());
        S3Object resultFileXml = value.stream()
            .map(x -> x.s3Object)
            .filter(x -> x.key().contains(".xml"))
            .findFirst().orElse(S3Object.builder().build());
        return new ReportFileResult(csvFile, resultFileHtml, resultFileXml);
    }

    private String getFirstPrefix(String key) {
        String[] s = key.split("_");
        return s[0];
    }

    private static String fileName(String name) {
        if (name != null && name.lastIndexOf(".") != -1 && name.lastIndexOf(".") != 0)
            return name.substring(name.lastIndexOf(".") + 1);
        else
            return "";
    }
}
