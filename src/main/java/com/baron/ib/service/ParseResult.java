package com.baron.ib.service;

import com.baron.ib.model.DividendRaw;
import com.baron.ib.model.TradeRaw;

import java.util.List;

public class ParseResult {
    List<DividendRaw> dividendRaws;
    List<TradeRaw> tradeRaws;

    public ParseResult(List<DividendRaw> dividendRaws, List<TradeRaw> tradeRaws) {
        this.dividendRaws = dividendRaws;
        this.tradeRaws = tradeRaws;
    }

    public List<DividendRaw> dividendRaws() {
        return dividendRaws;
    }

    public List<TradeRaw> tradeRaws() {
        return tradeRaws;
    }
}
