package com.baron.ib.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class CollectionService {
    public static final String BARON_COLLECTIONS = "baron-collections";

    private final S3Service s3service;
    private final ObjectMapper objectMapper;

    public CollectionService(S3Service s3service, ObjectMapper objectMapper) {
        this.s3service = s3service;
        this.objectMapper = objectMapper;
    }


    @PostConstruct
    public void init() {
        s3service.createBucket(BARON_COLLECTIONS);
    }


    public void saveRawCollection(byte[] bytes, String collectionName) {
        s3service.upload(bytes, collectionName, BARON_COLLECTIONS);
    }

    public byte[] getRawCollection(String collectionName) {
        return s3service.download(collectionName, BARON_COLLECTIONS);
    }

    public <T> T getCollection(String collectionName, TypeReference<T> typeReference) {
        byte[] download = s3service.download(collectionName + ".json", BARON_COLLECTIONS);
        try {
            T t = objectMapper.readValue(download, typeReference);
            return t;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveCollection(String collectionName, Object collection) {
        try {
            s3service.upload(objectMapper.writeValueAsBytes(collection), collectionName + ".json", BARON_COLLECTIONS);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
