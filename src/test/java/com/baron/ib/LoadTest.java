package com.baron.ib;

import com.baron.ib.model.Rate;
import com.baron.ib.model.Trade;
import com.baron.ib.model.TradeSummary;
import com.baron.ib.service.Parser;
import com.baron.ib.service.UtilRate;
import freemarker.template.TemplateException;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

class LoadTest {

    @Test
    void charset() throws IOException {
        byte[] bytes1 = Files.readAllBytes(Paths.get("/Users/dbaron/IdeaProjects/ib/src/test/resources/b.html"));
        String s = new String(bytes1);
        System.out.printf("");

    }

    @Test
    void loadKurs() {
        LocalDate now = LocalDate.now();
        String currency = "USD";
        var re = UtilRate.getRate(now, currency);
        System.out.println(re);
    }


    @Test
    void loadTestDividends() throws URISyntaxException {
        String file = "U4456953_2020_2020.csv";
        URI uri = Parser.class.getClassLoader()
            .getResource(file).toURI();

    }

    @Test
    void TradeSummary() {
        var list = List.of(
            new Trade("USD", "IAU", LocalDateTime.now().plusDays(-20), BigDecimal.valueOf(100), BigDecimal.valueOf(17.73), null, null, BigDecimal.valueOf(-1), null, BigDecimal.ZERO, null, null, new Rate(BigDecimal.valueOf(28.1375), "USD")),
            new Trade("USD", "IAU", LocalDateTime.now().plusDays(-10), BigDecimal.valueOf(-100), BigDecimal.valueOf(18.02), null, null, BigDecimal.valueOf(-1.0517242), null, BigDecimal.valueOf(14.448275), null, null, new Rate(BigDecimal.valueOf(28.2605), "USD")),
            new Trade("USD", "IAU", LocalDateTime.now().plusDays(-30), BigDecimal.valueOf(300), BigDecimal.valueOf(17.86), null, null, BigDecimal.valueOf(-1.5), null, BigDecimal.ZERO, null, null, new Rate(BigDecimal.valueOf(28.2673), "USD")),
            new Trade("USD", "IAU", LocalDateTime.now().plusDays(-9), BigDecimal.valueOf(-100), BigDecimal.valueOf(18.03), null, null, BigDecimal.valueOf(-1.0517463), null, BigDecimal.valueOf(15.448254), null, null, new Rate(BigDecimal.valueOf(28.2605), "USD")),
            new Trade("USD", "IAU", LocalDateTime.now().plusDays(4), BigDecimal.valueOf(-100), BigDecimal.valueOf(18.12), null, null, BigDecimal.valueOf(-1.0519452), null, BigDecimal.valueOf(24.448055), null, null, new Rate(BigDecimal.valueOf(28.2746), "USD"))
        );
        var trades = List.of(new Trade("USD", "PLTR", "2020-11-24T12:33:54", 6, 23.16, 23.82, -138.96, -1, 139.96, 0, 3.96, "O", new Rate(BigDecimal.valueOf(28.3694), "USD")),
            new Trade("USD", "PLTR", "2020-12-08T10:08:23", 10, 28.14, 28.59, -281.4, -1, 282.4, 0, 4.5, "O", new Rate(BigDecimal.valueOf(28.2394), "USD")),
            new Trade("USD", "PLTR", "2020-12-21T14:56:15", -16, 27.85, 28.51, 445.6, -1.01175176, -422.36, 22.228248, -10.56, "C", new Rate(BigDecimal.valueOf(27.828), "USD")));
        new TradeSummary("PLTR", trades, 2020);


    }


    @Test
    void loadTestTrade() throws TemplateException, IOException {
        String file = "U4456953_2020_2020.csv";
        String[] args = {"-f", file};
        //IbApplication.main();

    }


}
