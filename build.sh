#!/bin/bash
#[! -d .builder ] && mkdir .builder
CURRENT_DATETIME=$(date +%y%m%d-%H%M)
echo ${CURRENT_DATETIME}
cd  .builder
rm -rf application dependencies snapshot-dependencies spring-boot-loader
java -Djarmode=layertools -jar ../target/ib-0.0.1.jar extract
DOCKER_BUILDKIT=1  docker build -f ../docker/Dockerfile -t ib:${CURRENT_DATETIME} .

docker tag ib:${CURRENT_DATETIME} bar0n/ib:${CURRENT_DATETIME}
#docker login --username="bar0n" docker.io
docker push bar0n/ib:${CURRENT_DATETIME}


